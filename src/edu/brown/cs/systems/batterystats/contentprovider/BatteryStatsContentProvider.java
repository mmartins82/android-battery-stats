package edu.brown.cs.systems.batterystats.contentprovider;

import java.util.Arrays;
import java.util.HashSet;

import edu.brown.cs.systems.batterystats.database.BatteryStatsDatabaseHelper;
import edu.brown.cs.systems.batterystats.database.BatteryStatsTable;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/**
 *
 * @author Marcelo Martins <martins@cs.brown.edu>
 *
 */
public class BatteryStatsContentProvider extends ContentProvider {
    private static final String TAG = "BatteryStatsDataContentProvider";

    // database
    private BatteryStatsDatabaseHelper database;

    // Used for the UriMatcher
    private static final int BATTSTATS = 10;
    private static final int BATTSTAT_ID = 20;
    private static final int BATTSTAT_APPLIST = 30;
    private static final int BATTSTAT_LOADFACTOR = 40;

    private static final String AUTHORITY = "edu.brown.cs.systems.batterystats.contentprovider";

    private static final String BASE_PATH = "battstats";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/battstats";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/battstat";

    private static final UriMatcher sURIMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, BATTSTATS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", BATTSTAT_ID);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", BATTSTAT_APPLIST);
    }

    /**
     *
     * Initializes the provider by creating a new DatabaseHelper. onCreate() is called
     * automatically when Android creates the provider in response to a
     * resolver request from a client.
     */
    @Override
    public boolean onCreate() {
        // Created a new helper object. Note that the database itself
        // isn't opened until something tries to access it. and it's
        // only created if it doesn't already exist.
        database = new BatteryStatsDatabaseHelper(getContext());

        // Assumes that any failures will be reported by a thrown exception.
        return false;
    }


    /**
     * This method is called when a client calls
     * @link android.content.ContentResolver#query(Uri, String[], String, String[], String)}.
     * Queries the database and returns a cursor containing the results.
     *
     * @return A cursor containing the results of the query. The cursor exists but is empty if
     * the query returns no results or an exception occurs.
     * @throws IllegalArgumentException if the incoming URI pattern is invalid.
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        // Using SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // Check if the caller has requested a column which does not exist
        checkColumns(projection);

        // Set the table
        queryBuilder.setTables(BatteryStatsTable.TABLE_BATTSTATS);

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
        case BATTSTATS:
            break;

        case BATTSTAT_ID:
            // Adding the ID to the original query
            queryBuilder.appendWhere(BatteryStatsTable.COLUMN_ID + "="
                    + uri.getLastPathSegment());
            break;

        case BATTSTAT_APPLIST:
            // Adding app list to the original query
            queryBuilder.appendWhere(BatteryStatsTable.COLUMN_APPLIST + "="
                    + uri.getLastPathSegment());
            break;

        case BATTSTAT_LOADFACTOR:
            // Adding battery load factor to the original query
            queryBuilder.appendWhere(BatteryStatsTable.COLUMN_LOADFACTOR + "="
                    + uri.getLastPathSegment());
            break;

        default:
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getReadableDatabase();

        Cursor cursor = queryBuilder.query(
            db,              // database to query
            projection,      // columns to return from query
            selection,       // columns for the where clause
            selectionArgs,   // values for the where clause
            null,            // don't group by rows
            null,            // don't filter by row groups
            sortOrder        // sort order
        );

        // Make sure potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    /**
     * This is called when a client calls {@link android.content.ContentResolver#getType(Uri)}.
     * Returns the MIME data type of the URI given as a parameter.
     *
     * @param uri The URI whose MIME type is desired.
     * @return The MIME type of the URI.
     * @throws IllegalArgumentException if the incoming URI pattern is invalid.
     */
    @Override
    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case BATTSTATS:
                return CONTENT_TYPE;
            case BATTSTAT_ID:
                return CONTENT_ITEM_TYPE;
            case BATTSTAT_APPLIST:
                return CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unsupport URI: " + uri);
        }
    }

    /**
     * This is called when a client calls
     * {@link android.content.ContentResolver#insert(Uri, ContentValues)}.
     * Inserts a new row into the database. This method sets up default values for any
     * columns that are not included in the incoming map.
     * If rows were inserted, then listeners are notified of the change.
     * @return The row ID of the inserted row.
     * @throws SQLException if the insertion fails.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id = 0;

        // Validates the incoming URI. Only the full provider URI is allowed for inserts
        switch (uriType) {
        case BATTSTATS:
            id = sqlDB.insert(BatteryStatsTable.TABLE_BATTSTATS, null, values);
            Log.d(TAG, "Inserted batt stat (" + values.toString() + ")");
            break;

        default:
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        // If the insert succeeded, the row ID exists.
        if (id > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
            return Uri.parse(BASE_PATH + "/" + id);
        }

        // If the insert didn't succeed, then id <= 0. Throws exception
        throw new SQLiteException("Failed to insert row into " + uri);
    }

    /**
     * This is called when a client calls
     * {@link android.content.ContentResolver#delete(Uri, String, String[])}.
     * Deletes records from the database. If the incoming URI matches the note ID URI pattern,
     * this method deletes the one record specified by the ID in the URI. Otherwise, it deletes a
     * a set of records. The record or records must also match the input selection criteria
     * specified by where and whereArgs.
     *
     * If rows were deleted, then listeners are notified of the change.
     * @return If a "where" clause is used, the number of rows affected is returned, otherwise
     * 0 is returned. To delete all rows and get a row count, use "1" as the where clause.
     * @throws IllegalArgumentException if the incoming URI pattern is invalid.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;

        switch (uriType) {
        // Delete based on "where" columns and arguments.
        case BATTSTATS:
            rowsDeleted = sqlDB.delete(
                BatteryStatsTable.TABLE_BATTSTATS,    // DB table name
                selection,                            // where clause column names
                selectionArgs                         // where clause values
            );
            break;

        // Delete based on particular column ID
        case BATTSTAT_ID:
            String id = uri.getLastPathSegment();
            Log.d(TAG, "Deleting batt stats with id " + id);

            if (TextUtils.isEmpty(selection)) {
                rowsDeleted = sqlDB.delete(
                    BatteryStatsTable.TABLE_BATTSTATS,    // DB table name
                    BatteryStatsTable.COLUMN_ID +         // column ID
                    "=" +                                 // test for equality
                    id,    null
                );
            } else {
                rowsDeleted = sqlDB.delete(
                    BatteryStatsTable.TABLE_BATTSTATS,
                    BatteryStatsTable.COLUMN_ID +
                    "=" +
                    id + " and " +
                    selection,
                    selectionArgs);
            }
            break;

            // Delete based on particular column app list
            case BATTSTAT_APPLIST:
                String appList = uri.getLastPathSegment();
                Log.d(TAG, "Deleting batt stats with appList " + appList);

                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(
                        BatteryStatsTable.TABLE_BATTSTATS,    // DB table name
                        BatteryStatsTable.COLUMN_APPLIST +    // column APPLIST
                        "=" +                                 // test for equality
                        appList,
                        null
                    );
                } else {
                    rowsDeleted = sqlDB.delete(
                        BatteryStatsTable.TABLE_BATTSTATS,
                        BatteryStatsTable.COLUMN_APPLIST +
                        "=" +
                        appList + " and " +
                        selection,
                        selectionArgs);
                }
                break;

        default:
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        // Notifies observers registered against this provider that the data changed.
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    /**
     * This is called when a client calls
     * {@link android.content.ContentResolver#update(Uri,ContentValues,String,String[])}
     * Updates records in the database. The column names specified by the keys in the values map
     * are updated with new data specified by the values in the map. If the incoming URI matches the
     * note ID URI pattern, then the method updates the one record specified by the ID in the URI;
     * otherwise, it updates a set of records. The record or records must match the input
     * selection criteria specified by where and whereArgs.
     * If rows were updated, then listeners are notified of the change.
     *
     * @param uri The URI pattern to match and update.
     * @param values A map of column names (keys) and new values (values).
     * @param where An SQL "WHERE" clause that selects records based on their column values. If this
     * is null, then all records that match the URI pattern are selected.
     * @param whereArgs An array of selection criteria. If the "where" param contains value
     * placeholders ("?"), then each placeholder is replaced by the corresponding element in the
     * array.
     * @return The number of rows updated.
     * @throws IllegalArgumentException if the incoming URI pattern is invalid.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated = 0;

        switch (uriType) {
        // Update based on incoming data
        case BATTSTATS:
            rowsUpdated = sqlDB.update(
                BatteryStatsTable.TABLE_BATTSTATS,    // DB table name
                values,                               // map of column names and new values
                selection,                            // where clause column names
                selectionArgs                         // where clause values
            );
            break;

        // Update restrict to particular ID
        case BATTSTAT_ID:
            String id = uri.getLastPathSegment();
            Log.d(TAG, "Updating batt stats (" + id + ", " + values.toString()
                    + ")");

            if (TextUtils.isEmpty(selection)) {
                rowsUpdated = sqlDB.update(
                    BatteryStatsTable.TABLE_BATTSTATS,
                    values,
                    BatteryStatsTable.COLUMN_ID +
                    "=" +
                    id, null
                );
            } else {
                rowsUpdated = sqlDB.update(
                    BatteryStatsTable.TABLE_BATTSTATS,    // DB tables name
                    values,                               // map of columns names and new values
                    BatteryStatsTable.COLUMN_ID +         // name of column ID
                    "=" +                                 // equality test
                    id    +                               // value of ID
                    " and " +
                    selection,                            // where clause column names
                    selectionArgs                         // where clause values
                );
            }
            break;

            // Update restrict to particular ID
            case BATTSTAT_APPLIST:
                String appList = uri.getLastPathSegment();
                Log.d(TAG, "Updating batt stats (" + appList + ", " + values.toString()
                        + ")");

                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(
                        BatteryStatsTable.TABLE_BATTSTATS,
                        values,
                        BatteryStatsTable.COLUMN_APPLIST +
                        "=" +
                        appList,
                        null
                    );
                } else {
                    rowsUpdated = sqlDB.update(
                        BatteryStatsTable.TABLE_BATTSTATS,    // DB tables name
                        values,                               // map of columns names and new values
                        BatteryStatsTable.COLUMN_APPLIST +    // name of column APPLIST
                        "=" +                                 // equality test
                        appList +                             // value of APPLIST
                        " and " +
                        selection,                            // where clause column names
                        selectionArgs                         // where clause values
                    );
                }
                break;
        default:
            throw new IllegalArgumentException("Unknown URI:" + uri);
        }

        // Validates the incoming URI. Only the full provider URI is allowed for inserts
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        String[] available = { BatteryStatsTable.COLUMN_ID,
                BatteryStatsTable.COLUMN_APPLIST,
                BatteryStatsTable.COLUMN_LOADFACTOR };

        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(
                    Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(
                    Arrays.asList(available));

            // Check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException(
                        "Unknow colums in projection");
            }
        }
    }
}
