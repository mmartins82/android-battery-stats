package edu.brown.cs.systems.batterystats.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Wrapper functions to SQLite DB creation
 *
 * @author Marcelo Martins <martins@cs.brown.edu>
 *
 */
public class BatteryStatsDatabaseHelper extends SQLiteOpenHelper {

    public static final String TABLE_BATTSTATS = "battstats";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_APPLIST = "appList";
    public static final String COLUMN_LOADFACTOR = "loadFactor";

    private static final String DATABASE_NAME = "battstats.db";
    private static final int DATABASE_VERSION = 1;

    public BatteryStatsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method is called during database creation
    @Override
    public void onCreate(SQLiteDatabase database) {
        BatteryStatsTable.onCreate(database);
    }

    // Method is called during database upgrade,
    // e.g., if you increase database version
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
            int newVersion) {
        BatteryStatsTable.onUpgrade(database, oldVersion, newVersion);
    }
}
