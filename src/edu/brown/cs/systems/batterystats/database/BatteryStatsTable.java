package edu.brown.cs.systems.batterystats.database;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * SQLite access to stats database
 *
 * @author Marcelo Martins <martins@cs.brown.edu>
 *
 */
public class BatteryStatsTable {

    private final static String TAG = "BatteryStatsTable";
    // Database table
    public static final String TABLE_BATTSTATS = "battstats";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_APPLIST = "appList";
    public static final String COLUMN_LOADFACTOR = "loadFactor";

    // Database creation SQL statement

    private static final String DATABASE_CREATE = "CREATE TABLE "
            + TABLE_BATTSTATS + "(" + COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_APPLIST
            + " TEXT NOT NULL, " + COLUMN_LOADFACTOR + " DOUBLE"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
            int newVersion) {
        Log.w(TAG,
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_BATTSTATS);
        onCreate(database);
    }
}
