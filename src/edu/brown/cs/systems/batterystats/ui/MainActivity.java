package edu.brown.cs.systems.batterystats.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import android.widget.ToggleButton;
import edu.brown.cs.systems.batterystats.R;
import edu.brown.cs.systems.batterystats.service.BatteryStatsService;

/**
 * @author martins
 * 
 */
public class MainActivity extends Activity {

    ToggleButton toggleButton;
    private static final String TAG = "MainActivity";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_screen);

        toggleButton = (ToggleButton) findViewById(R.id.toggleButton1);
        toggleButton.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.toggleButton1:
                        if (toggleButton.isChecked()) {
                            stopService(new Intent(MainActivity.this,
                                    BatteryStatsService.class));
                            Toast.makeText(MainActivity.this,
                                    "Battery service enabled",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            startService(new Intent(MainActivity.this,
                                    BatteryStatsService.class));
                            Toast.makeText(MainActivity.this,
                                    "Battery service disabled",
                                    Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    /** Called when the activity is destroyed. */
    @Override
    protected void onDestroy() {

        super.onDestroy();
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton1);

        if (toggleButton.isChecked()) {
            stopService(new Intent(MainActivity.this, BatteryStatsService.class));
        }

        Log.d(TAG, "onDestroy()");
    }

}
