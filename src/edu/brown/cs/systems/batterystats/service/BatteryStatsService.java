package edu.brown.cs.systems.batterystats.service;

import edu.brown.cs.systems.batterystats.contentprovider.BatteryStatsContentProvider;
import edu.brown.cs.systems.batterystats.database.BatteryStatsTable;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

/**
 * @author Marcelo Martins <martins@cs.brown.edu>
 * 
 */
public class BatteryStatsService extends Service {

    private static final String TAG = "BatteryStatsService";
    private Handler serviceHandler = null;
    private Task battStatus = null;
    private int updatePeriod = 120;         // load-factor calc rate (in seconds)

    public int getUpdatePeriod() {
        return updatePeriod;
    }

    public void setUpdatePeriod(int updatePeriod) {
        this.updatePeriod = updatePeriod;
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.app.Service#onBind(android.content.Intent)
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private final IBatteryStatsService.Stub mBinder = new IBatteryStatsService.Stub() {

        /**
         * Return the batt load factor compared to idle given a appList key
         * 
         * @param appList
         * @return load factor corresponding to app list
         */
        public float getBatteryLoadFactor(String appList) {

            String[] projection = { BatteryStatsTable.COLUMN_APPLIST,
                    BatteryStatsTable.COLUMN_LOADFACTOR };
            float load = 0;
            BatteryStatsContentProvider provider = new BatteryStatsContentProvider();
            Cursor cursor = provider.query(Uri.withAppendedPath(
                    BatteryStatsContentProvider.CONTENT_URI, appList),
                    projection, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();
                load = cursor
                        .getFloat(cursor
                                .getColumnIndexOrThrow(BatteryStatsTable.COLUMN_LOADFACTOR));
            }

            return load;
        }

        /**
         * Get battery remaining hours from current running profile
         * 
         * @return
         */
        public int getBatteryRemainingHours() {
            // TODO: Retrieve remaining hours from current running profile
            return 0;
        }

        /**
         * Get batt remaining hours given an appList key (profile)
         * 
         * @param appList
         * @return
         */
        public int estimateRemainingHours(String appList) {

            float load = getBatteryLoadFactor(appList);
            // TODO: Convert to load to hours
            return 0;
        }
    };

    public void onCreate() {
        Log.d(getClass().getSimpleName(), "onCreate()");
        battStatus = new Task();
    }

    public void onStart(Intent intent, int id) {
        serviceHandler = new Handler();
        serviceHandler.postDelayed(battStatus, 1L);
        Log.d(TAG, "onStart()");
    }

    public void onDestroy() {
        serviceHandler.removeCallbacks(battStatus);
        serviceHandler = null;
        Log.d(TAG, "onDestroy()");
    }

    private void onUpdate(int discharge) {
        // TODO: Fill in stub
    }

    class Task implements Runnable {

        public void run() {
            updateBattDischarge();
            serviceHandler.postDelayed(this, updatePeriod);
            Log.i(TAG, "Incrementing counter in the run method");
        }

        private static final String TAG = "BatteryStatusReceiver";
        private int status = BatteryManager.BATTERY_STATUS_DISCHARGING;
        private int prevLevel = Integer.MAX_VALUE;
        private int level = 0;
        private int battDiff = 0;

        void updateBattDischarge() {

            Intent intent = getApplicationContext().registerReceiver(null,
                    new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            // Register application when it's installed
            if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {

                status = intent.getIntExtra(BatteryManager.EXTRA_STATUS,
                        BatteryManager.BATTERY_HEALTH_UNKNOWN);

                int holder = level;

                if (status == BatteryManager.BATTERY_STATUS_DISCHARGING) {

                    level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                    if (prevLevel < level) {
                        battDiff = (level - prevLevel);
                        prevLevel = holder;
                        onUpdate(battDiff);
                    }
                }
            }
        }
    }
}
