package edu.brown.cs.systems.batterystats.service;

interface IBatteryStatsService {
    /**
     * Return the batt load factor compared to idle given a appList key
     * @param appList
     * @return load factor corresponding to app list
     */
    float getBatteryLoadFactor(String appList);

    /**
     * Get battery remaining hours from current running profile
     * @return
     */
    int getBatteryRemainingHours();

    /**
     * Get batt remaining hours given an appList key (profile)
     * @param appList
     * @return 
     */
    int estimateRemainingHours(String appList);
}